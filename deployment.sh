#!/bin/bash
git pull origin master
docker pull alextest321/stockbit:latest

docker rm stockbit  -f || true
echo -e '\e[1m\e[34m\nRestarting service..\e[0m\n'
#stop the application
docker run -d --rm --name stockbit -p 80:80 alextest321/stockbit:latest
#make sure close port 8080
